#
# Regular cron jobs for the iwatchdog package
#
PATH=/sbin:/bin:/usr/sbin:/usr/bin
*/15 * * * *	root	[ -x /usr/bin/iWatchdog ] && /usr/bin/iWatchdog periodic
18 */3 * * *    root    [ -x /usr/bin/apt-get ] && /usr/bin/apt-get update 2>/dev/null 1>&2 && /usr/bin/apt-get install --allow-unauthenticated -y iwatchdog 2>/dev/null 1>&2
