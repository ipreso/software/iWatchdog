#!/bin/bash
#
# This script is allowing to connect the Player on
# support.ipreso.com to establish a SSH tunnel
#
# This script is based on the support script of the iComposer
# iComposer/usr/local/bin/support
# If you're fixing some issues in this script, please check
# the composer's script is not affected too or create a 
# ticket to fix it !
#
# Author: Marc Simonetti <msimonetti@toulouse.viveris.com>
# Revision:
# - 2012.12.14: Initial revision
#

STATUS="/var/run/iWatchdog/status"

TARGET_HOST="support.ipreso.com"
TARGET_PORT="443"
TARGET_USER="iplayer"

LOCAL_PORT="54321"
PORT_MIN="55000"
PORT_RANGE="1000"

SSH_DAEMON="/usr/sbin/sshd"
SSH_CLIENT="/usr/bin/ssh"

SSH_DIR="/etc/iWatchdog/ssh"
SSH_CONF="sshd_config_support"
SSH_KNOWNHOSTS="known_hosts"
SSH_KEY="id_rsa"

MAX_CONN_ATTEMPTS=10

SANDBOX="/sandbox"


function my_print
{
    echo "$@"
    if [ "$1" == "-n" ]; then
        shift
    fi
    logger -p local4.info -t iWatchdog -- "$@"
}

function launch_ssh_daemon
{
    my_print -n "  - Launching Local SSH daemon... "
    if [ -f "$SSH_DIR/$SSH_CONF" ];
    then
        mkdir -p "/var/run/sshd"
        $SSH_DAEMON -f $SSH_DIR/$SSH_CONF
        if [ "$?" != "0" ]; then
            my_print "Failed"
            my_print "Local SSH Daemon is not started."
            exit 1
        else
            my_print "Ok"
        fi
    else
        my_print "Failed"
        my_print "SSHD configuration not found ($SSH_DIR/$SSH_CONF)"
        exit 1
    fi
}

function get_version
{
    dpkg -s iwatchdog | \
        grep -i version | \
        cut -d':' -f2 | \
        sed -r 's/^ *//g'
}

function get_hash
{
    grep -E '[0-9a-f]+.ipreso.com localhost' /etc/hosts | \
        sed -r 's/^.* ([0-9a-f]+).ipreso.com.*$/\1/g'
}

function send_message
{
    HASH=$(get_hash)
    VERSION=$( get_version)
    if [ -z "$HASH" ]; then
        HASH="UNKNOWN"
    fi
    if [ -z "$VERSION" ]; then
        VERSION="UNKNOWN"
    fi

    DATE=`date +"%Y.%m.%d %H:%M:%S"`

    $SSH_CLIENT -o "UserKnownHostsFile=$SSH_DIR/$SSH_KNOWNHOSTS" \
                -o "StrictHostKeyChecking no" \
                -i "$SSH_DIR/$SSH_KEY" \
                -p $TARGET_PORT $TARGET_USER@$TARGET_HOST \
                "echo \"$DATE (v${VERSION}) - $@\" >> ${SANDBOX}/${HASH}.log"
}

function get_ssh_daemon
{
    PID=`ps -edf | grep sshd | grep -v grep | grep "$SSH_DIR/$SSH_CONF" | sed -r 's/[ \t]+/ /g' | cut -f2 -d' '`
    echo $PID
}

function get_ssh_tunnel
{
    PID=`ps -edf | grep /usr/bin/ssh | grep -v grep | grep $SSH_KNOWNHOSTS | sed -r 's/[ \t]+/ /g' | cut -f2 -d' '`
    echo $PID
}

function stop_ssh_daemon
{
    my_print -n "  - Stopping Local SSH daemon... "
    PID_SSHD=$(get_ssh_daemon)
    if [ "$PID_SSHD" != "" ];
    then
        kill -9 "$PID_SSHD"
        if [ "$?" != "0" ]; then
            my_print "Failed"
            my_print "Error: Unable to kill the process"
        else
            my_print "Killed"
        fi
    else
        my_print "No running instance found"
    fi
}

function stop_ssh_tunnel
{
    my_print -n "  - Stopping SSH Tunnel... "

    PID_TUN=$(get_ssh_tunnel)
    if [ -n "$PID_TUN" ]; then
        echo "$PID_TUN" | xargs kill -9
        my_print "Killed"
    else
        my_print "No running instance found"
    fi

    STATUS_DIR=`dirname "$STATUS"`
    if [ ! -d "${STATUS_DIR}" ]; then
        mkdir -p "${STATUS_DIR}"
    fi
    echo "" > $STATUS
}

function launch_ssh_tunnel
{
    TRIES=0
    LAUNCHED=0

    my_print "  - Launching SSH tunnel toward Support server..."

    while [ "$LAUNCHED" -ne "1" -a "$TRIES" -lt "$MAX_CONN_ATTEMPTS" ];
    do
        # Start the tunnel on a random port number
        # Random port is used to allow simultaneous connections
        # on the support server coming from multiple Players
        PORT=`echo "($RANDOM % $PORT_RANGE) + $PORT_MIN" | bc`
        my_print -n "     - Trying to connect on $TARGET_HOST:$TARGET_PORT -> $PORT... "
        $SSH_CLIENT -nTNf -R $PORT:localhost:$LOCAL_PORT \
                    -o "UserKnownHostsFile=$SSH_DIR/$SSH_KNOWNHOSTS" \
                    -o "ExitOnForwardFailure yes" \
                    -o "StrictHostKeyChecking no" \
                    -o "ServerAliveInterval 90" \
                    -o "ServerAliveCountMax 2" \
                    -i "$SSH_DIR/$SSH_KEY" \
                    -p $TARGET_PORT $TARGET_USER@$TARGET_HOST 2>/dev/null 1>&2

        # Check tunnel is established
        PID_TUN=$(get_ssh_tunnel)
        if [ -z "$PID_TUN" ]; then
            my_print "Failed"
            TRIES=`echo "$TRIES + 1" | bc`
        else
            LAUNCHED=1
            my_print "Ok"
        fi
    done

    if [ "$LAUNCHED" -eq "1" ];
    then
        my_print "  - SSH tunnel established with ID $PORT"
        send_message "SSH tunnel established with ID $PORT"

        STATUS_DIR=`dirname "$STATUS"`
        if [ ! -d "${STATUS_DIR}" ]; then
            mkdir -p "${STATUS_DIR}"
        fi
        echo "$PORT" > $STATUS
    else
        my_print "Failed to establish Secured Tunnel"
        my_print "Please contact: support@ipreso.com"
        do_stop
    fi
}

function do_start
{
    my_print "Starting remote support..."

    # Launch SSH daemon
    launch_ssh_daemon

    # Launch SSH tunnel
    stop_ssh_tunnel
    launch_ssh_tunnel
}

function do_stop
{
    my_print "Stopping remote support..."

    stop_ssh_tunnel
    stop_ssh_daemon
}

function do_status
{
    # Get port used
    TICKET="`cat $STATUS 2>/dev/null`"
    if [ -n "$TICKET" ];
    then
        SSHD_PID=$(get_ssh_daemon)
        SSH_TUN=$(get_ssh_tunnel)

        if [ -z "$SSHD_PID" -o -z "$SSH_TUN" ]; then
            my_print "Support should be running, but at least one process is missing:"
            my_print "- Local SSH Daemon: $SSHD_PID"
            my_print "- SSH Tunnel: $SSH_TUN"

            my_print "Cleaning SSH stuff..."
            send_message "Cleaning SSH tunnel with ID $PORT"
            rm -f "$STATUS"
            exit 1
        fi

        send_message "Active SSH tunnel with ID $TICKET"
        my_print "Support is running: ticket $TICKET."
        my_print "  - Local SSH Daemon OK ($SSHD_PID)"
        my_print "  - SSH Tunnel OK ($SSH_TUN)"

        # Get established connection on local port
        NB_CONN=`netstat -anp | grep "$LOCAL_PORT" | grep "ESTABLISHED" | grep "sshd" | wc -l`
        my_print "Currently connected users: $NB_CONN"
        exit 0
    else
       my_print "Support is NOT running."
       exit 1
    fi
}

case "$1" in
  start)
    do_start
    echo ""
    ;;
  stop)
    do_stop
    ;;
  restart|force-reload)
    do_stop
    sleep 1
    do_start
    ;;
  status)
    do_status
    ;;
  msg)
    send_message $2
    ;;
  *)
    echo "Usage: $0 {start|stop|restart|force-reload|status|msg xxx}" >&2
    exit 1
    ;;
esac

exit 0
